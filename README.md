## ![](logo.png)


L'application simule un site e-commerce avec 3 scénarios d'utilisation, un client, un utilisateur classique coté entreprise et un administrateur avec des droits elargis.
Elle a été developpé en C# avec ASP.Net pour la partie Back-end, Razor pour la partie Vue et enfin SQL Server pour la partie Bdd.

🚧 &nbsp; Le projet est toujours en cours de developpement &nbsp;  🚧 

<e>Les fonctionnalités en cours de réalisation et celles terminées ou developpées seront précisées ci-après.<e>


## Description
![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)

### Les fonctionnalités 

- [X] Afficher les catégories de produit.             
- [X] Les produits sont affichés avec leurs caractéristiques, seuls ou par catégorie. 
- [X] Les produits sont enregistrables dans un pannier (session).
- [X] Creation Produit, Utilisateur (admin) et categorie de produit.
- [X] Les produits peuvent devenir des favoris et intégrer une liste de favoris (session) et de sortir de cette liste.
- [X] Transformation du panier en commande.
- [X] Rechercher un produit par son nom ou sa description.
- [X] Les produits sont affichés avec leurs caractéristiques ( prix, descrption, ean, intitulé, photos, etc.), seuls ou par catégorie. 
- [X] Les produits sont enregistrables dans un pannier (session).
- [X] Les produits peuvent devenir des favoris et intégrer une liste de favoris (session).
- [X] Choix des quantités du produit sélectionné.
- [X] Transformation du panier en commande.
- [X] Vidage du panier complet ou par produit.
- [ ] Suppression d'un produit / Panier / Commande.
- [X] Etape de connexion par login / etape de creation de login et informations par profil utilisateur.
- [X] Accés différenciés aux fonctionnalités en fonction du profil.
- [X] Affichage du nom ou prenom de l'utilisateur dans la barre de menu.
- [ ] Traduction complete de l'application en anglais et français (partiel).
- [ ] Possibilité de noter un produit.
- [ ] Possibilité d'effectuer un commmentaire sur un produit.
- [X] Transformation du panier en commande.
- [X] Modification de l'etat d'une commande avec sa date et la personne ayant réalisée l'action.
- [X] Communiation par message entre Client et Entreprise.
- [X] Recherche d'une commande par son etat, son numero ou nom du client.
- [X] Accès à l'historique des commandes.


![image](/images/image1.png) 
![image](/images/image2.png) 
![image](/images/image5.png) 
![image](/images/image4.png) 
![image](/images/image3.png)



