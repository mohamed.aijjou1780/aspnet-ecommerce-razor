﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class MoyenPaiement
    {

        private int id;
        private string moyen;

        public int Id { get => id; set => id = value; }
        public string Moyen { get => moyen; set => moyen = value; }
    }
}
