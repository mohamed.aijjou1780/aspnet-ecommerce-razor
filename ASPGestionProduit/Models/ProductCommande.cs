﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class ProductCommande
    {
        private int id;
        private Product product;
        private int qte;

        public int Id { get => id; set => id = value; }
        public Product Product { get => product; set => product = value; }
        public int Qte { get => qte; set => qte = value; }



        public ProductCommande()
        {




        }
    }
}
