﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class Commentaire
    {
        private int id;
        private string message;
        private Product product;
        private Utilisateur utilisateur;
        private DateTime date;

        public int Id { get => id; set => id = value; }
        public string Message { get => message; set => message = value; }
        public Product Product { get => product; set => product = value; }
        public Utilisateur Utilisateur { get => utilisateur; set => utilisateur = value; }
        public DateTime Date { get => date; set => date = value; }

        public Commentaire() { }


    }
}
