﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class MessagesCommandes
    {

        private int id;
        private Commande commande;
        private Utilisateur utilisateur;
        private string message;
        private DateTime date;

        public int Id { get => id; set => id = value; }
        public Commande Commande { get => commande; set => commande = value; }
        public Utilisateur Utilisateur { get => utilisateur; set => utilisateur = value; }
        public string Message { get => message; set => message = value; }
        public DateTime Date { get => date; set => date = value; }


        public MessagesCommandes()
        {

        }
    }
}
