﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    //[JsonObject(IsReference = true)]
    public class Categorie
    {

        private int id;
        private string nom;
        //private List<Product> products;
        private ImageCategorie imageCategorie;



        [Key]
        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }

        //[System.Text.Json.Serialization.JsonIgnore]
        //[IgnoreDataMember]
        //public List<Product> Products { get => products; set => products = value; }
        public ImageCategorie ImageCategorie { get => imageCategorie; set => imageCategorie = value; }

        public Categorie() { }


    }
}
