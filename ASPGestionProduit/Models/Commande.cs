﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class Commande
    {

        private int id;

        public int Id { get => id; set => id = value; }


        public List<ProductCommande> ProductCommande { get; set; }

        public Utilisateur Utilisateur { get; set; }

        public int Qte { get; set; }

        public DateTime Date { get; set; }

        public decimal Montant { get; set; }

        public MoyenPaiement MoyenPaiement { get; set; }

        public Commande()
        {
            ProductCommande = new List<ProductCommande>();

        }

    }
}
