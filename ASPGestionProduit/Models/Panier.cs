﻿using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class Panier
    {
        private int id;
        private List<ProductPanier> productPaniers;
        private static int index;

        public int Id { get => id; set => id = value; }
        public List<ProductPanier> ProductPaniers { get => productPaniers; set => productPaniers = value; }


       public Panier()
        {
            Id = index++;

            ProductPaniers = new List<ProductPanier>();

        }



        public void AddProduct(Product product, int qty)
        {
            bool found = false;
            ProductPaniers.ForEach((p) =>
            {
                if (p.Product.Id == product.Id)
                {
                    p.Qty += qty;
                    found = true;
                }
            });
            if (!found)
            {
                ProductPanier productCart = new ProductPanier() { Product = product, Qty = qty };
                ProductPaniers.Add(productCart);
            }
        }



    }
}
