﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class HistoriqueEtatCommandes
    {

        private int id;
        private DateTime dateEtatCommande;
        private int numeroCommande;
        private string etatCommande;
        private string nomEmploye;

        public int Id { get => id; set => id = value; }
        public DateTime DateEtatCommande { get => dateEtatCommande; set => dateEtatCommande = value; }
        public int NumeroCommande { get => numeroCommande; set => numeroCommande = value; }
        public string EtatComande { get => etatCommande; set => etatCommande = value; }
        public string NomEmploye { get => nomEmploye; set => nomEmploye = value; }

        public HistoriqueEtatCommandes()
        {

            DateEtatCommande = new DateTime();
        }


    }
}
