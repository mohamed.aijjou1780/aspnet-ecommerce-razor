﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class Etat
    {

        private int id;
        private string statut;

        public int Id { get => id; set => id = value; }
        public string Statut { get => statut; set => statut = value; }

        public Etat() { }
            

    }
}
