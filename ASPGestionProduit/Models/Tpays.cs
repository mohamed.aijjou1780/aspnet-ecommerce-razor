﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class Tpays
    {
        private int id;
        private string paysName;
        private string paysCode;

        public int Id { get => id; set => id = value; }
        public string PaysName { get => paysName; set => paysName = value; }
        public string PaysCode { get => paysCode; set => paysCode = value; }


        public Tpays()
        {

        }



    }
}
