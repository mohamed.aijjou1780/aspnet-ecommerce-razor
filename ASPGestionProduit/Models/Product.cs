﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{


    //[DataContract(IsReference = true)]
    public class Product
    {

        int id;
        long ean;
        string titre;
        decimal prix;
        string description;
        private bool isFav;
        List<Image> images;
        private Categorie categorie;
        //private List<Commentaire> commentaires;





        public int Id { get => id; set => id = value; }
        public string Titre { get => titre; set => titre = value; }
        public decimal Prix { get => prix; set => prix = value; }
       public string Description { get => description; set => description = value; }
        public List<Image> Images { get => images; set => images = value; }

        public Categorie Categorie { get => categorie; set => categorie = value; }
   
        //public List<Commentaire> Commentaires { get => commentaires; set => commentaires = value; }
        public bool IsFav { get => isFav; set => isFav = value; }
        public long Ean { get => ean; set => ean = value; }

        public Product(long ean = 3980000000000)
        {
            Random rand1 = new Random();
            Random rand2 = new Random();
            long val1 = rand1.Next(1, 10000);
            long val2 = rand2.Next(1, 10000);
            Ean = ean + (val1 * val2);
            Images = new List<Image>();
        }



    }
}
