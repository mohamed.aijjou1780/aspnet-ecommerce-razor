﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class Utilisateur
    {

        private int id;
        private string nom;
        private string prenom;
        private string adresse;
        private string ville;
        private string codePostal;
        private Tpays pays;
        private string identifiant;
        private string mdp;
        private Role role;
        private List<Commentaire> commenatires;

   

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }
        public string Identifiant { get => identifiant; set => identifiant = value; }
        public string Mdp { get => mdp; set => mdp = value; }
        public Role Roles { get => role; set => role = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
        public string CodePostal { get => codePostal; set => codePostal = value; }
        public List<Commentaire> Commenatires { get => commenatires; set => commenatires = value; }

        public Tpays Tpays { get => pays; set => pays = value; }

        public Utilisateur()
        {

        }
    }
}
