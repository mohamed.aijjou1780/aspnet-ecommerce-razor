﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class NoteProduct
    {

        private int id;
        private Product product;
        private Note note;

        public int Id { get => id; set => id = value; }
        public Product Product { get => product; set => product = value; }
        public Note Note { get => note; set => note = value; }


        public NoteProduct()
        {



        }


    }
}
