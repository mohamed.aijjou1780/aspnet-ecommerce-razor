﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class Image
    {
        int id;
        string url;
        int productId;

        public int Id { get => id; set => id = value; }
        public string Url { get => url; set => url = value; }
        public int ProductId { get => productId; set => productId = value; }

        public Image()
        {


        }

        public Image(string url, int idProdcut)
        {
            Url = url;
            ProductId = idProdcut;

        }


    }
}