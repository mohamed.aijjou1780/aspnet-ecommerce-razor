﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Models
{
    public class EtatCommande
    {
        private int id;
        private DateTime date;
        private Etat etat;
        private Commande commande;

        public int Id { get => id; set => id = value; }
        public DateTime Date { get => date; set => date = value; }
        public Etat Etat { get => etat; set => etat = value; }
        public Commande Commande { get => commande; set => commande = value; }



        public EtatCommande() { }

    }
}
