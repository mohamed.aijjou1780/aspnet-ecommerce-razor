﻿using ASPGestionProduit.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Policy;
using System.Threading.Tasks;

namespace ASPGestionProduit.Tools
{
    public class DataContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private static DataContext _instance;
        private static object _lock = new object();


        public static DataContext Instance
        {
           get
            {
               lock (_lock)
               {
                    if(_instance == null)
                    

                       _instance = new DataContext();
                        return _instance;
                    
                }
            }
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<ProductCommande> ProductCommandes { get; set; }
        public DbSet<Commande> Commandes { get; set; }

        public DbSet<Etat> Etats { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<EtatCommande> EtatCommandes { get; set; }

        public DbSet<Categorie> Catagories { get; set; }

        public DbSet<ImageCategorie> ImageCategories { get; set; }

        public DbSet<MoyenPaiement> MoyenPaiements { get; set; }

        public DbSet<Tpays> Tpays { get; set; }

        public DbSet<MessagesCommandes> MessagesCommandes { get; set; }
        public DbSet<HistoriqueEtatCommandes> HistoriqueEtatCommandes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {

            dbContextOptionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");


        }

        public DataContext() : base()
        {

        }


    }
}
