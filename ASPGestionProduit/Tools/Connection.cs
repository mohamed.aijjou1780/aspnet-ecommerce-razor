﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ASPGestionProduit.Tools
{
    class Connection
    {
        private static SqlConnection _instance;
       private static Mutex m = new Mutex();
        public static SqlConnection Instance
        {
            get
            {
               m.WaitOne();
               if (_instance == null)
               {
                    _instance = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                    // CreateTable();
                }
                m.ReleaseMutex();
                return _instance;
            }
        }

        private Connection()
        {

        }
    }

}
