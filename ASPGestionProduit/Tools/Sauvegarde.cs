﻿using ASPGestionProduit.Controllers;
using ASPGestionProduit.Models;
using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Tools
{
    public class Sauvegarde
    {


        private static Sauvegarde _instance = null;
        private static SqlCommand command;
        private static SqlDataReader reader;
        public static Sauvegarde Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Sauvegarde();
                return _instance;
            }
        }
        private Sauvegarde()
        {

        }

        public Role GetRoleById( int id)
        {

            return DataContext.Instance.Roles.Find(id);
        }


        public List<Etat> GetEtat()
        {


            return DataContext.Instance.Etats.ToList();
        }


        public Tpays GetPaysById(int id)
        {


            return DataContext.Instance.Tpays.Find(id);
        }

        public Commande CommandeById(int id)
        { 

            return DataContext.Instance.Commandes.Find(id);
        }

    public List<MoyenPaiement> GetMoyenPaiements()
        {
            return DataContext.Instance.MoyenPaiements.ToList();
        }

        public MoyenPaiement GetMoyenPaiementById(int id)
        {

            return DataContext.Instance.MoyenPaiements.Find(id);


        }

        public Commande GetCommandeByIdCommand(int id)
        {
            return DataContext.Instance.Commandes.Include(p => p.ProductCommande).ThenInclude(u => u.Product).FirstOrDefault(p => p.Id == id);
        }
        
        public List<EtatCommande> GetCommandeByEtat(string etat)
        {
            List<EtatCommande>listes = DataContext.Instance.EtatCommandes.Include((e) => e.Commande).ThenInclude((c)=>c.Utilisateur).Include((e)=>e.Commande).ThenInclude((m)=>m.MoyenPaiement).Include((e) => e.Etat).Where((et) => et.Etat.Statut == etat).ToList();

            return listes;
        }

        public List<EtatCommande> SearchCommandByEtat(string nom)
        {

            List<EtatCommande> listes = DataContext.Instance.EtatCommandes.Include((e) => e.Commande).ThenInclude((c) => c.Utilisateur).Include((e) => e.Commande).ThenInclude((m) => m.MoyenPaiement).Include((e) => e.Etat).Where((e)=>e.Commande.Utilisateur.Nom.Contains(nom)).ToList();

            return listes;

        }

        public List<EtatCommande> SearchCommandByEtatId(int id)
        {

            List<EtatCommande> listes = DataContext.Instance.EtatCommandes.Include((e) => e.Commande).ThenInclude((c) => c.Utilisateur).Include((e) => e.Commande).ThenInclude((m) => m.MoyenPaiement).Include((e) => e.Etat).Where((e) => e.Commande.Id == id).ToList();

            return listes;

        }

        public List<Categorie> GetCategories()
        {
            return DataContext.Instance.Catagories.Include(c=>c.ImageCategorie).ToList();
        }

        public bool AddCategorie(Categorie categorie)
        {
            DataContext.Instance.Catagories.Add(categorie);
            DataContext.Instance.SaveChanges();
        
            return true;
        }


        public List<Product> GetProductsByCategorie(int idCategorie)
        {
            return DataContext.Instance.Products.Include((p) => p.Images).Where((p) => p.Categorie.Id == idCategorie).ToList();
        }

        public Categorie GetCategorieById(int idCategorie)
        {


            return DataContext.Instance.Catagories.Include((c)=> c.ImageCategorie).FirstOrDefault((c)=>c.Id == idCategorie);
        }

        public Product GetProdutcById(int idProduct)
        {
            return DataContext.Instance.Products.Include((p)=>p.Images).Include((p)=>p.Categorie).FirstOrDefault((p)=>p.Id == idProduct);
        }


        public List<Tpays> GetListPays()
        {
            return DataContext.Instance.Tpays.ToList();
        }

        public List<Role> GetRoles()
        {
            return DataContext.Instance.Roles.ToList();
        }

        public EtatCommande GetEtatCommandeById(int id)
        {
            return DataContext.Instance.EtatCommandes.Find(id);
        }

        public Etat GetEtatById(int id)
        {
            return DataContext.Instance.Etats.Find(id);
        }

        public List<Product> GetProducts()
        {
         
            return DataContext.Instance.Products.Include(p => p.Images).Include(p=> p.Categorie).ToList();
        }

       public List<HistoriqueEtatCommandes> GetHistoriqueEtatCommandesByIdCommand(int id)
        {

            return DataContext.Instance.HistoriqueEtatCommandes.Where(h => h.NumeroCommande == id).ToList();
        }

        public List<Commande> GetCommandesByUser(string identifiant, string prenom)
        {
            return DataContext.Instance.Commandes.Include((pp) => pp.ProductCommande).Include(c => c.MoyenPaiement).Where(c => c.Utilisateur.Identifiant == identifiant && c.Utilisateur.Prenom == prenom).ToList();

        }

        public List<MessagesCommandes> GetListMessagesByIdCommande(int idCommande)
        {
            return DataContext.Instance.MessagesCommandes.Include(u=> u.Utilisateur).ThenInclude(r => r.Roles).Include(c => c.Commande).Where(c => c.Commande.Id == idCommande).ToList();
        }

        public Utilisateur GetUtilisateurByEmail(string email)
        {
            return DataContext.Instance.Utilisateurs.FirstOrDefault((u) => u.Identifiant == email);

        }
        public bool Addproduct(Product product)
        {
           
            DataContext.Instance.Products.Add(product);
            DataContext.Instance.SaveChanges();
            return product.Id > 0;
        }

        public bool AddImage(Image image)
        {
            DataContext.Instance.Images.Add(image);
            DataContext.Instance.SaveChanges();
            return image.Id > 0;


        }

        public  List<Product> SearchProductByMessage(string message)
        {  
            List<Product> products = DataContext.Instance.Products.Include((p)=>p.Images)/*.Include(p=>p.Categorie).*/.Where((p) => p.Titre.Contains(message) || p.Description.Contains(message)).ToList();

            return products;

        }

        //public bool delete(Product product)
        //{
        //    string request = "delete from product where id=@idProduct";
        //    command = new SqlCommand(request, Connection.Instance);
        //    command.Parameters.Add(new SqlParameter("@idProduct", product.Id));
        //    Connection.Instance.Open();
        //    command.Dispose();
        //    int nbre = command.ExecuteNonQuery();
        //    Connection.Instance.Close();
        //    return nbre>0;
        //}


        //public bool deleteImage(int idProduct)
        //{
        //    string request = "delete from image where idProduct=@idProduct";
        //    command = new SqlCommand(request, Connection.Instance);
        //    command.Parameters.Add(new SqlParameter("@idProduct", idProduct));
        //    Connection.Instance.Open();
        //    command.Dispose();
        //    int nbre = command.ExecuteNonQuery();
        //    Connection.Instance.Close();
        //    return nbre > 0;
        //}


        //public bool Update(Product product)
        //{


        //    string request = "UPDATE product set titre=@titre, prix=@prix, description=@description " +
        //    "where id=@idProduit";
        //    command = new SqlCommand(request, Connection.Instance);
        //    command.Parameters.Add(new SqlParameter("@titre", product.Titre));
        //    command.Parameters.Add(new SqlParameter("@prix", product.Prix));
        //    command.Parameters.Add(new SqlParameter("@description", product.Description));
        //    command.Parameters.Add(new SqlParameter("@idProduit", product.Id));
        //    Connection.Instance.Open();
        //    int nbRow = command.ExecuteNonQuery();
        //    command.Dispose();
        //    Connection.Instance.Close();
        //    return nbRow == 1;



        //}



        public string Traduction(string message, string langue)
        {
            string nouveauMessage = " ";

            string request = "SELECT valeur FROM traduction where mot=@mot and langue=@langue";
            command = new SqlCommand(request, Connection.Instance);
            command.Parameters.Add(new SqlParameter("@mot", message));
            command.Parameters.Add(new SqlParameter("@langue", langue));
            Connection.Instance.Open();
            reader = command.ExecuteReader();
            if (reader.Read())
            {
                nouveauMessage = reader.GetString(0);

            }
            reader.Close();
            command.Dispose();
            Connection.Instance.Close();
            return nouveauMessage;


        }



    }



}
