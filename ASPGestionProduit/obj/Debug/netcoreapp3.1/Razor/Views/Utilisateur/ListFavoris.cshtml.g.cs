#pragma checksum "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "640edbf33b1f89e4f99d94d38881beb761fecd0a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Utilisateur_ListFavoris), @"mvc.1.0.view", @"/Views/Utilisateur/ListFavoris.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\_ViewImports.cshtml"
using ASPGestionProduit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\_ViewImports.cshtml"
using ASPGestionProduit.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\_ViewImports.cshtml"
using ASPGestionProduit.Interface;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"640edbf33b1f89e4f99d94d38881beb761fecd0a", @"/Views/Utilisateur/ListFavoris.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8a2135bc28a5f05b27f655a86ac54e563e34be0e", @"/Views/_ViewImports.cshtml")]
    public class Views_Utilisateur_ListFavoris : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "DetailProduct", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Utilisateur", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddFavori", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-warning"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<div class=\"container\">\r\n    <h5 class=\"m-3\">Liste des favoris</h5>\r\n");
#nullable restore
#line 3 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
     if (Model.Count > 0)
    {
        

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
         foreach (Product p in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"row justify-content-lg-around mb-4\">\r\n                <div class=\"col\">\r\n                    <img");
            BeginWriteAttribute("src", " src=\"", 274, "\"", 296, 1);
#nullable restore
#line 9 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
WriteAttributeValue("", 280, p.Images[0].Url, 280, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" style=""height:25vh; width:20vw;"" />
                </div>
                <div class=""col card w-75 pl-0 pr-0 justify-content-around"">
                    <div class=""card-header bg-info w-100 m-0 p-2"">
                        <div class=""row pl-4 pr-4"">
                            <div class=""row"">
                                <div class=""row m-1  w-100"">
                                    <h4>");
#nullable restore
#line 16 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
                                   Write(p.Categorie.Nom);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h4> \r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <h4 class=\"card-title\">");
#nullable restore
#line 22 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
                                          Write(p.Titre);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h4>\r\n                        <h5 class=\"card-text\">");
#nullable restore
#line 23 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
                                         Write(p.Ean);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n                       <h5 class=\"card-text\">");
#nullable restore
#line 24 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
                                        Write(p.Prix);

#line default
#line hidden
#nullable disable
            WriteLiteral(" <i class=\"fas fa-euro-sign\"></i></h5>\r\n                        <p class=\"card-text\">");
#nullable restore
#line 25 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
                                        Write(p.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                    </div>\r\n                    <div class=\"row justify-content-around mb-4\">\r\n                        <div class=\"justify-content-center\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "640edbf33b1f89e4f99d94d38881beb761fecd0a8899", async() => {
                WriteLiteral("Ajouter Panier");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 28 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
                                                                                                                  WriteLiteral(p.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</div>\r\n                        <div class=\"justify-content-center\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "640edbf33b1f89e4f99d94d38881beb761fecd0a11515", async() => {
                WriteLiteral("Supprimer");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 29 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
                                                                                                                     WriteLiteral(p.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n");
#nullable restore
#line 34 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 34 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
         
    }
    else
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"row\">\r\n            <div class=\"col alert-danger alert\">\r\n                Pas de favori pour le moment\r\n            </div>\r\n        </div>\r\n");
#nullable restore
#line 43 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Utilisateur\ListFavoris.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
