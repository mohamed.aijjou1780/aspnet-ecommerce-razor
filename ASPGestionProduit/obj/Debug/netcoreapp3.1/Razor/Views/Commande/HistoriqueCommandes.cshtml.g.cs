#pragma checksum "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a1756de1414849cf78d134007f169b2df8036af8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Commande_HistoriqueCommandes), @"mvc.1.0.view", @"/Views/Commande/HistoriqueCommandes.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\_ViewImports.cshtml"
using ASPGestionProduit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\_ViewImports.cshtml"
using ASPGestionProduit.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\_ViewImports.cshtml"
using ASPGestionProduit.Interface;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a1756de1414849cf78d134007f169b2df8036af8", @"/Views/Commande/HistoriqueCommandes.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8a2135bc28a5f05b27f655a86ac54e563e34be0e", @"/Views/_ViewImports.cshtml")]
    public class Views_Commande_HistoriqueCommandes : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<HistoriqueEtatCommandes>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Commande", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "GetHistoriqueCommandes", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            WriteLiteral("<div class=\"container-fluid\" style=\"display:flex; flex-direction:column;\">\r\n    <div class=\"container mt-3\">\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a1756de1414849cf78d134007f169b2df8036af84392", async() => {
                WriteLiteral(@"
            <div class=""row"">
                <div class=""input-group mb-3 col"">
                    <input type=""text"" name=""id"" class=""form-control"" placeholder=""Saisir le numero de commande"" aria-describedby=""basic-addon2"">
                    <div class=""input-group-append"">
                        <Button class=""input-group-text btn btn-info"" type=""submit"" id=""basic-addon2"">Rechercher</Button>
                    </div>
                </div>
            </div>
        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        <small id=\"passwordHelpBlock\" class=\"form-text text-muted\">\r\n            Vous pouvez obtenir l\'historique d\'une commande par son numero.\r\n        </small>\r\n    </div>\r\n");
#nullable restore
#line 19 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
     if (Model != null)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"        <table class=""table table-info mt-4"">
            <thead>
                <tr>
                    <th scope=""col"">N°Commande</th>
                    <th scope=""col"">Date Etat</th>
                    <th scope=""col"">Utilisateur</th>
                    <th scope=""col"">Statut</th>
                  
                </tr>
            </thead>
            <tbody>
");
#nullable restore
#line 32 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
                 foreach (HistoriqueEtatCommandes h in @Model)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr>\r\n                        <td>");
#nullable restore
#line 35 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
                       Write(h.NumeroCommande);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 36 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
                       Write(h.DateEtatCommande);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 37 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
                       Write(h.NomEmploye);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 38 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
                       Write(h.EtatComande);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    </tr>\r\n");
#nullable restore
#line 40 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </tbody>\r\n            <tfoot>\r\n            </tfoot>\r\n        </table>\r\n");
#nullable restore
#line 45 "C:\Users\Administrateur\source\repos\ASPGestionProduit\ASPGestionProduit\Views\Commande\HistoriqueCommandes.cshtml"
    }
    else { }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<HistoriqueEtatCommandes>> Html { get; private set; }
    }
}
#pragma warning restore 1591
