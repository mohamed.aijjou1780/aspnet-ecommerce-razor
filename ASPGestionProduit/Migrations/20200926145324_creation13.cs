﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class creation13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Catagorie_CategorieId",
                table: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Catagorie",
                table: "Catagorie");

            migrationBuilder.RenameTable(
                name: "Catagorie",
                newName: "Catagories");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Catagories",
                table: "Catagories",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Etats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Statut = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Etats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EtatCommandes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(nullable: false),
                    EtatId = table.Column<int>(nullable: true),
                    CommandeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EtatCommandes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EtatCommandes_Commandes_CommandeId",
                        column: x => x.CommandeId,
                        principalTable: "Commandes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EtatCommandes_Etats_EtatId",
                        column: x => x.EtatId,
                        principalTable: "Etats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EtatCommandes_CommandeId",
                table: "EtatCommandes",
                column: "CommandeId");

            migrationBuilder.CreateIndex(
                name: "IX_EtatCommandes_EtatId",
                table: "EtatCommandes",
                column: "EtatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Catagories_CategorieId",
                table: "Products",
                column: "CategorieId",
                principalTable: "Catagories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Catagories_CategorieId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "EtatCommandes");

            migrationBuilder.DropTable(
                name: "Etats");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Catagories",
                table: "Catagories");

            migrationBuilder.RenameTable(
                name: "Catagories",
                newName: "Catagorie");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Catagorie",
                table: "Catagorie",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Catagorie_CategorieId",
                table: "Products",
                column: "CategorieId",
                principalTable: "Catagorie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
