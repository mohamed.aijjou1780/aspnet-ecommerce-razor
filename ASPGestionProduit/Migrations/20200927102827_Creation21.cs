﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class Creation21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catagories_Images_ImageId",
                table: "Catagories");

            migrationBuilder.DropIndex(
                name: "IX_Catagories_ImageId",
                table: "Catagories");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Catagories");

            migrationBuilder.AddColumn<int>(
                name: "ImageCategorieId",
                table: "Catagories",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ImageCategorie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: true),
                    CategorieId1 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageCategorie", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Catagories_ImageCategorieId",
                table: "Catagories",
                column: "ImageCategorieId");

            migrationBuilder.AddForeignKey(
                name: "FK_Catagories_ImageCategorie_ImageCategorieId",
                table: "Catagories",
                column: "ImageCategorieId",
                principalTable: "ImageCategorie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catagories_ImageCategorie_ImageCategorieId",
                table: "Catagories");

            migrationBuilder.DropTable(
                name: "ImageCategorie");

            migrationBuilder.DropIndex(
                name: "IX_Catagories_ImageCategorieId",
                table: "Catagories");

            migrationBuilder.DropColumn(
                name: "ImageCategorieId",
                table: "Catagories");

            migrationBuilder.AddColumn<int>(
                name: "ImageId",
                table: "Catagories",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Catagories_ImageId",
                table: "Catagories",
                column: "ImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Catagories_Images_ImageId",
                table: "Catagories",
                column: "ImageId",
                principalTable: "Images",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
