﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class creation11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategorieId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Catagorie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catagorie", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategorieId",
                table: "Products",
                column: "CategorieId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Catagorie_CategorieId",
                table: "Products",
                column: "CategorieId",
                principalTable: "Catagorie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Catagorie_CategorieId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "Catagorie");

            migrationBuilder.DropIndex(
                name: "IX_Products_CategorieId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CategorieId",
                table: "Products");
        }
    }
}
