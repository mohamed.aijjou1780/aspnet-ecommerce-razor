﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class Creation22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catagories_ImageCategorie_ImageCategorieId",
                table: "Catagories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ImageCategorie",
                table: "ImageCategorie");

            migrationBuilder.RenameTable(
                name: "ImageCategorie",
                newName: "ImageCategories");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ImageCategories",
                table: "ImageCategories",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Catagories_ImageCategories_ImageCategorieId",
                table: "Catagories",
                column: "ImageCategorieId",
                principalTable: "ImageCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catagories_ImageCategories_ImageCategorieId",
                table: "Catagories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ImageCategories",
                table: "ImageCategories");

            migrationBuilder.RenameTable(
                name: "ImageCategories",
                newName: "ImageCategorie");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ImageCategorie",
                table: "ImageCategorie",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Catagories_ImageCategorie_ImageCategorieId",
                table: "Catagories",
                column: "ImageCategorieId",
                principalTable: "ImageCategorie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
