﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class Creazion301 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistoriqueEtatCommandes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateEtatCommande = table.Column<DateTime>(nullable: false),
                    NumeroCommande = table.Column<int>(nullable: false),
                    EtatComande = table.Column<string>(nullable: true),
                    NomEmploye = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoriqueEtatCommandes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoriqueEtatCommandes");
        }
    }
}
