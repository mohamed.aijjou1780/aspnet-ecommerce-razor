﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class creation8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Adresse",
                table: "Utilisateurs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodePostal",
                table: "Utilisateurs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ville",
                table: "Utilisateurs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adresse",
                table: "Utilisateurs");

            migrationBuilder.DropColumn(
                name: "CodePostal",
                table: "Utilisateurs");

            migrationBuilder.DropColumn(
                name: "Ville",
                table: "Utilisateurs");
        }
    }
}
