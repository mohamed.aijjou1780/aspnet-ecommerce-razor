﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class Creatdi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TpaysId",
                table: "Utilisateurs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Utilisateurs_TpaysId",
                table: "Utilisateurs",
                column: "TpaysId");

            migrationBuilder.AddForeignKey(
                name: "FK_Utilisateurs_Tpays_TpaysId",
                table: "Utilisateurs",
                column: "TpaysId",
                principalTable: "Tpays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Utilisateurs_Tpays_TpaysId",
                table: "Utilisateurs");

            migrationBuilder.DropIndex(
                name: "IX_Utilisateurs_TpaysId",
                table: "Utilisateurs");

            migrationBuilder.DropColumn(
                name: "TpaysId",
                table: "Utilisateurs");
        }
    }
}
