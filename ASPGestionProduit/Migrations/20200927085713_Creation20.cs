﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class Creation20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImageId",
                table: "Catagories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Catagories_ImageId",
                table: "Catagories",
                column: "ImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Catagories_Images_ImageId",
                table: "Catagories",
                column: "ImageId",
                principalTable: "Images",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Catagories_Images_ImageId",
                table: "Catagories");

            migrationBuilder.DropIndex(
                name: "IX_Catagories_ImageId",
                table: "Catagories");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Catagories");
        }
    }
}
