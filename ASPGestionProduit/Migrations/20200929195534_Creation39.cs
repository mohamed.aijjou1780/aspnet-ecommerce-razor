﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class Creation39 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MoyenPaiementId",
                table: "Commandes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MoyenPaiements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Moyen = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoyenPaiements", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Commandes_MoyenPaiementId",
                table: "Commandes",
                column: "MoyenPaiementId");

            migrationBuilder.AddForeignKey(
                name: "FK_Commandes_MoyenPaiements_MoyenPaiementId",
                table: "Commandes",
                column: "MoyenPaiementId",
                principalTable: "MoyenPaiements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commandes_MoyenPaiements_MoyenPaiementId",
                table: "Commandes");

            migrationBuilder.DropTable(
                name: "MoyenPaiements");

            migrationBuilder.DropIndex(
                name: "IX_Commandes_MoyenPaiementId",
                table: "Commandes");

            migrationBuilder.DropColumn(
                name: "MoyenPaiementId",
                table: "Commandes");
        }
    }
}
