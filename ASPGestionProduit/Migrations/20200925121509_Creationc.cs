﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPGestionProduit.Migrations
{
    public partial class Creationc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commandes_ProductCommandes_ProductCommandeId",
                table: "Commandes");

            migrationBuilder.DropIndex(
                name: "IX_Commandes_ProductCommandeId",
                table: "Commandes");

            migrationBuilder.DropColumn(
                name: "ProductCommandeId",
                table: "Commandes");

            migrationBuilder.AddColumn<int>(
                name: "CommandeId",
                table: "ProductCommandes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductCommandes_CommandeId",
                table: "ProductCommandes",
                column: "CommandeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductCommandes_Commandes_CommandeId",
                table: "ProductCommandes",
                column: "CommandeId",
                principalTable: "Commandes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductCommandes_Commandes_CommandeId",
                table: "ProductCommandes");

            migrationBuilder.DropIndex(
                name: "IX_ProductCommandes_CommandeId",
                table: "ProductCommandes");

            migrationBuilder.DropColumn(
                name: "CommandeId",
                table: "ProductCommandes");

            migrationBuilder.AddColumn<int>(
                name: "ProductCommandeId",
                table: "Commandes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Commandes_ProductCommandeId",
                table: "Commandes",
                column: "ProductCommandeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Commandes_ProductCommandes_ProductCommandeId",
                table: "Commandes",
                column: "ProductCommandeId",
                principalTable: "ProductCommandes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
