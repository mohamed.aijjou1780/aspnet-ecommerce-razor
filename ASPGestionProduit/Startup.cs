using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPGestionProduit.Interface;
using ASPGestionProduit.Services;
using ASPGestionProduit.Tools;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ASPGestionProduit
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment _env)
        {
            Configuration = configuration; 
           
        }
       

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddScoped<ITraduction, TraductionService>();
            services.AddScoped<ILoger, LogServicecs>();
            services.AddScoped<IHash, HashService>();
            services.AddScoped<IFavori, FavoriService>();
            services.AddScoped<IUploadFile, UploadFileService>();
            services.AddScoped<IPanier, PanierService>();
            services.AddAuthorization(options =>
            {
                options.AddPolicy("connectOk", policy =>
                {
                    //policy.RequireClaim(ClaimTypes.Email);
                    policy.Requirements.Add(new ConnectRequirement());
                });
                options.AddPolicy("admin", policy =>
                {
                    //policy.RequireClaim(ClaimTypes.Email);
                    policy.Requirements.Add(new ConnectRequirement("admin"));
                });
            });

            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = true;
            });

            services.AddScoped<IAuthorizationHandler, ConnectHandler>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = new PathString("/Utilisateur/FormLogin");
                options.AccessDeniedPath = new PathString("/Utilisateur/Denied");
                options.ExpireTimeSpan = TimeSpan.FromDays(1);
            });
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(60);
            });
            services.AddHttpContextAccessor();
            services.AddMvc();





        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseAuthentication();
            app.UseRouting();
        

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                   name: "Ajout",
                   pattern: "{controller=Home}/{action=AjoutQuantite}/{id?}/{qte}");
                endpoints.MapControllerRoute(
                  name: "Panier",
                  pattern: "{controller=Panier}/{action=AddProduct}/{id?}");
            });
        }
    }
}
