﻿using ASPGestionProduit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Interface
{
  public interface IPanier
    {

        public Panier GetPanier();



        bool RemovePanier();


        bool RemoveProductPanier(ProductCommande productCommande);



        Panier AddPanier(ProductPanier productPanier, Panier panier);

        void UpdatePanier(Panier panier);



    }
}
