﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Interface
{
    public interface IFavori
    {
        bool AddToFavoris(int id);
        List<int> getFavoris();

        bool RemoveFromFavoris(int id);



    }
}
