﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using ASPGestionProduit.Interface;
using ASPGestionProduit.Models;
using ASPGestionProduit.Tools;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ASPGestionProduit.Controllers
{
    public class UtilisateurController : Controller
    {

        private IHash _hash;
        private IFavori _fav;


        public UtilisateurController(IHash hash, IFavori fav)
        {

            _hash = hash;
            _fav = fav;
        }
        public IActionResult SignInForm()
        {

            List<Tpays> pays = Sauvegarde.Instance.GetListPays();

            ViewBag.pays = pays;
            List<Role> roles = Sauvegarde.Instance.GetRoles();

            return View(roles);
        }




        [HttpPost]
        public IActionResult SubmitSignIn([FromForm] Utilisateur utilisateur, int idRole, int idTpays)
        {
            Role role;

            if (idRole == 0)
            {

                role = new Role();
                role.Roles = Sauvegarde.Instance.GetRoleById(1).Roles;

            }else
            {

                role = Sauvegarde.Instance.GetRoleById(idRole);

            }
           
            Tpays pays = Sauvegarde.Instance.GetPaysById(idTpays);

            

            utilisateur.Tpays = pays;
            utilisateur.Roles = role;
            utilisateur.Mdp = _hash.GetHash(SHA256.Create(), utilisateur.Mdp);
            DataContext.Instance.Utilisateurs.Add(utilisateur);
            DataContext.Instance.SaveChanges();
            return RedirectToAction("FormLogin");
        }

        public IActionResult FormLogin()
        {
           

            return View();
        }

        




        [HttpPost]
        public async Task<IActionResult> SubmitLogin(string identifiant, string mdp)
        {
            string compareHash = _hash.GetHash(SHA256.Create(), mdp);
            Utilisateur utilisateur = DataContext.Instance.Utilisateurs.Include(u => u.Roles).Include(u => u.Tpays).FirstOrDefault(u => u.Identifiant == identifiant && u.Mdp == compareHash);
            if (utilisateur != null)
            {
                //Garder la connexion dans les sessions, ainsi que les informations des utilisateurs
                //_login.SaveUser(utilisateur);
                //Utilisation du login avec AuthenticationCookie
                List<Claim> claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Email, utilisateur.Identifiant),
                    new Claim(ClaimTypes.Name , utilisateur.Prenom),
                    new Claim(ClaimTypes.Role, utilisateur.Roles.Roles),
                 
                };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                AuthenticationProperties option = new AuthenticationProperties()
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddDays(1)
                };
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), option);
                return RedirectToAction("Index", "Home");
            }
            return View("FormLogin");
        }

        public async Task<IActionResult> LogOut()
        {
            _fav.getFavoris().Clear();
            //HttpContext.Session.Clear();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Denied()
        {

            return View();

        }

        public IActionResult GetListFavori()
        {

            List<Product> productsFavoris = new List<Product>();
            foreach (int id in _fav.getFavoris())
            {
                Product p = Sauvegarde.Instance.GetProdutcById(id);
                productsFavoris.Add(p);
            }

            return View("ListFavoris", productsFavoris);
        }

        public IActionResult AddFavori(int id)
        {
            bool isFav = false;

            if (_fav.getFavoris().Count > 0)
            {
                foreach (int idFav in _fav.getFavoris())
                {
                    if (idFav == id)
                    {
                        isFav = true;
                        break;
                    }
                    else
                    {
                        isFav = false;
                    }
                }

                if (isFav == true)
                {
                    _fav.RemoveFromFavoris(id);
                    Product product = Sauvegarde.Instance.GetProdutcById(id);
                    product.IsFav = false;
                    DataContext.Instance.SaveChanges();
                }
                else
                {
                    _fav.AddToFavoris(id);
                    Product product = Sauvegarde.Instance.GetProdutcById(id);
                    product.IsFav = true;
                    DataContext.Instance.SaveChanges();
                }
            }
            else
            {

                _fav.AddToFavoris(id);
            }
            return RedirectToAction("GetListFavori");
        }

    }




}

