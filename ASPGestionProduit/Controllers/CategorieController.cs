﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using ASPGestionProduit.Interface;
using ASPGestionProduit.Models;
using ASPGestionProduit.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ASPGestionProduit.Controllers
{
    public class CategorieController : Controller
    {
        private IUploadFile _uploadFileService;


        public CategorieController(IUploadFile uploadFile)
        {

            _uploadFileService = uploadFile;

        }

        public IActionResult AfficheCreationCategorie()
        {
            return View();
        }

      


        public IActionResult SubmitCategorie(IFormFile image, string titre)
        {
            ImageCategorie imagee = new ImageCategorie() { Url = _uploadFileService.UploadFile(image) };
            Categorie categorie = new Categorie() { ImageCategorie = imagee, Nom = titre };
            Sauvegarde.Instance.AddCategorie(categorie);
            return View("AfficheCreationCategorie", categorie);
        }
    }
}
