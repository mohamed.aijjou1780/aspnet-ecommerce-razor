﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ASPGestionProduit.Models;
using ASPGestionProduit.Tools;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using ASPGestionProduit.Interface;
using Microsoft.AspNetCore.Authorization;

namespace ASPGestionProduit.Controllers
{
    public class HomeController : Controller
    {


        private IWebHostEnvironment env;

        private ITraduction traductionService;

        private ILoger _logger;

        private IFavori _fav;

        public HomeController(IWebHostEnvironment _env, ITraduction trad, ILoger ilogger, IFavori fav)
        {
            env = _env;
            traductionService = trad;
            _logger = ilogger;
            _fav = fav;
        }






        public IActionResult Index()
        {
            List<Categorie> categories = Sauvegarde.Instance.GetCategories();
            if (_fav.getFavoris().Count == 0)
            {

                foreach (Product p in Sauvegarde.Instance.GetProducts())
                {

                    if (p.IsFav == true)
                    {
                        p.IsFav = false;

                    }

                }

            }

            string langue = HttpContext.Session.GetString("langue");
            ViewBag.langue = langue;
            ViewBag.categories = categories;
            return View();

        }



        public IActionResult Detailproduct(int id)
        {

            string cartString = HttpContext.Session.GetString("seriePanier");

            ViewBag.cartstring = cartString;
            Product product = Sauvegarde.Instance.GetProdutcById(id);
            return View(product);

        }


        [Authorize(Policy = "admin")]
        public IActionResult FormProduct()
        {

            List<Categorie> categories = Sauvegarde.Instance.GetCategories();


            return View(categories);
        }


        [Authorize(Policy = "admin")]
        public IActionResult SubmitProduct(int id, string titre, int idCategorie, decimal prix, string description, List<IFormFile> images)
        {
            Product productTrouve = Sauvegarde.Instance.GetProdutcById(id);

            Categorie categorie = Sauvegarde.Instance.GetCategorieById(idCategorie);

            if (productTrouve == null)
            {

                List<Image> imagess = new List<Image>();

                Product product = new Product() { Titre = titre, Prix = prix, Description = description, Categorie = categorie };
                Sauvegarde.Instance.Addproduct(product);

                foreach (IFormFile i in images)
                {
                    string path = Path.Combine(env.WebRootPath, "upload", i.FileName);
                    Stream s = System.IO.File.Create(path);
                    i.CopyTo(s);
                    s.Close();

                    Image image = new Image() { Url = "https://localhost:44383/" + "upload/" + i.FileName, ProductId = product.Id };
                    imagess.Add(image);
                    Sauvegarde.Instance.AddImage(image);

                }

                product.Images = imagess;

                return RedirectToAction("Index");

            }
            else
            {

                productTrouve.Titre = titre;
                productTrouve.Prix = prix;
                productTrouve.Description = description;




                if (images != null)
                {
                    List<Image> imagess = new List<Image>();

                    foreach (IFormFile i in images)
                    {
                        string path = Path.Combine(env.WebRootPath, "upload", i.FileName);
                        Stream s = System.IO.File.Create(path);
                        i.CopyTo(s);
                        s.Close();
                        Image image = new Image() { Url = "https://localhost:44383/" + "upload/" + i.FileName, ProductId = productTrouve.Id };
                        imagess.Add(image);


                    }

                    productTrouve.Images = imagess;

                }
                else
                {
                    productTrouve.Images = Sauvegarde.Instance.GetProdutcById(productTrouve.Id).Images;

                }
                return RedirectToAction("Index");

            }

        }
        public IActionResult AddPanier(int id, int qte)
        {
            Product product = Sauvegarde.Instance.GetProdutcById(id);

            product.Images = Sauvegarde.Instance.GetProdutcById(product.Id).Images;

            if (HttpContext.Session.GetInt32("compteur") == null)
            {
                int compteur = 0;

                HttpContext.Session.SetInt32("compteur", (int)compteur);
                compteur += qte;
                ViewBag.compteur = compteur;
            }
            else
            {
                int compteur = (int)HttpContext.Session.GetInt32("compteur");
                compteur += qte;
                HttpContext.Session.SetInt32("compteur", (int)compteur);

                ViewBag.compteur = compteur;
            }
            return View("Detailproduct", product);
        }


        //public IActionResult SupprimerProduct(int id)
        //{
        //    Product product = Sauvegarde.Instance.GetProdutcById(id);

        //    if (Sauvegarde.Instance.deleteImage(product.Id)){

        //       if(Sauvegarde.Instance.delete(product))
        //        {

        //            return RedirectToAction("Index");
        //        }
        //    }

        //    return RedirectToAction("Index");
        //}

        //public IActionResult ModifierProduct(int id)
        //{
        //    Product product = Sauvegarde.Instance.GetProdutcById(id);
        //    List<Image> images = Sauvegarde.Instance.GetImagesByIdProduct(product.Id);

        //    if (product != null)
        //    {

        //        ViewBag.product = product;
        //        return View("FormProduct");
        //    }
        //    return RedirectToAction("Index");
        //}

        public IActionResult ChangementLangue(string langue)
        {
            HttpContext.Session.SetString("langue", langue);


            return RedirectToAction("Index");

        }

        public List<Product> AfficheListProduct()
        {
            List<Product> products = Sauvegarde.Instance.GetProducts();
            foreach (Product product in products)
            {
                product.Images = Sauvegarde.Instance.GetProdutcById(product.Id).Images;
            }

            foreach (Product p in Sauvegarde.Instance.GetProducts())
            {
                if (p.Images == null)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            return products;
        }

        public IActionResult RechercheUnProduct(string message)
        {


            List<Categorie> categories = Sauvegarde.Instance.GetCategories();



            try
            {
                List<Product> products = Sauvegarde.Instance.SearchProductByMessage(message);

                string langue = HttpContext.Session.GetString("langue");
                ViewBag.langue = langue;

                ViewBag.categories = categories;
                return View("Index", products);
            }
            catch (ArgumentOutOfRangeException e)
            {
                string error = e.Message;
                Console.WriteLine(error);
                _logger.Creationlog(error);
                return View("Error");
            }
        }

        public IActionResult ActionListProductByCategorie(int id)
        {
            List<Categorie> categories = Sauvegarde.Instance.GetCategories();
            List<Product> products = Sauvegarde.Instance.GetProductsByCategorie(id);


            ViewBag.categories = categories;
            return View("Index", products);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
