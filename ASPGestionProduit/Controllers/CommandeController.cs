﻿using ASPGestionProduit.Interface;
using ASPGestionProduit.Models;
using ASPGestionProduit.Tools;
using ASPGestionProduit.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ASPGestionProduit.Controllers
{
    public class CommandeController : Controller
    {

        private IPanier _servicePanier;
        private IHttpContextAccessor _accessor;


        public CommandeController(IHttpContextAccessor accessor, IPanier servicePabier)
        {
            _servicePanier = servicePabier;
            _accessor = accessor;

        }

        public IActionResult ValidationCommande(int idPaiement)
        {

            string p = HttpContext.Session.GetString("seriePanier");
            int quantiteProductCommand = 0;
            DateTime date = DateTime.Now;
            Decimal totalTotal = 0;

            List<ProductCommande> productCommandes = new List<ProductCommande>();
            List<Product> products = new List<Product>();
            Panier panier = JsonConvert.DeserializeObject<Panier>(p);
            MoyenPaiement moyenPaiement = Sauvegarde.Instance.GetMoyenPaiementById(idPaiement);

            foreach (ProductPanier pP in panier.ProductPaniers)
            {

                decimal total = pP.Qty * pP.Product.Prix;

                totalTotal += total;
                Product product = Sauvegarde.Instance.GetProdutcById(pP.Product.Id);
                ProductCommande productCommande = new ProductCommande() { Product = product, Qte = pP.Qty };
                productCommandes.Add(productCommande);
            };




            string emailUser = HttpContext.User.Claims.FirstOrDefault(u => u.Type == ClaimTypes.Email).Value;

            Utilisateur utilisateur = DataContext.Instance.Utilisateurs.FirstOrDefault(u => u.Identifiant == emailUser);

            Commande commande = new Commande() { ProductCommande = productCommandes, Utilisateur = utilisateur, MoyenPaiement = moyenPaiement, Date = date };



            foreach (ProductCommande pp in commande.ProductCommande)
            {
                quantiteProductCommand += pp.Qte;
            }

            commande.Qte = quantiteProductCommand;
            commande.Montant = totalTotal;

            DataContext.Instance.Commandes.Add(commande);

            DataContext.Instance.SaveChanges();

            Etat etat = new Etat();

            List<Etat> etats = Sauvegarde.Instance.GetEtat();

            foreach(Etat e in etats)
            {
                if(e.Statut == "Validée")
                {
                    etat = e;


                }


            }

            EtatCommande etatcommande = new EtatCommande() { Commande = commande, Date = date, Etat = etat };
            DataContext.Instance.EtatCommandes.Add(etatcommande);
            DataContext.Instance.SaveChanges();

            HistoriqueEtatCommandes historiqueEtatCommandes = new HistoriqueEtatCommandes() { DateEtatCommande = date, EtatComande = etat.Statut, NumeroCommande = commande.Id, NomEmploye = commande.Utilisateur.Nom };
            DataContext.Instance.HistoriqueEtatCommandes.Add(historiqueEtatCommandes);
            DataContext.Instance.SaveChanges();


            HttpContext.Session.Clear();


            return View("ListCommande", commande);
        }


        public IActionResult MesCommandes()
        {

            string identifiant = _accessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email).Value;
            string name = _accessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;


            List<Commande> commandes = Sauvegarde.Instance.GetCommandesByUser(identifiant, name);

            return View("MesCommandes", commandes);
        }


        public IActionResult GestionCommande()
        {

            List<Etat> etats = Sauvegarde.Instance.GetEtat();

            ViewBag.etats = etats;

            return View();
        }


        public IActionResult DetailsCommande(int id, bool modifie)
        {
            CommandeViewModel vm = new CommandeViewModel();

            if(modifie != false)
            {
                modifie = true;
            }else
            {
                modifie = false;
            }



            ViewBag.modifie = modifie;

          List<Etat> etats = Sauvegarde.Instance.GetEtat();

          List<EtatCommande> listesEtatsCommandes = Sauvegarde.Instance.SearchCommandByEtatId(id);

            List<HistoriqueEtatCommandes> historiqueEtatCommandes = Sauvegarde.Instance.GetHistoriqueEtatCommandesByIdCommand(id);

            List<MessagesCommandes> messagesCommandes = Sauvegarde.Instance.GetListMessagesByIdCommande(id);

            Commande commande = Sauvegarde.Instance.GetCommandeByIdCommand(id);

            vm.EtatCommandes = listesEtatsCommandes;

            vm.Etatss = etats;

            vm.historiqueEtatCommandes = historiqueEtatCommandes;

            vm.commande = commande;

            vm.messagesCommandes = messagesCommandes;

           return View(vm);

        }
        public IActionResult RechercheCommande(string nom)
        {


            List<EtatCommande> etatCommandes = Sauvegarde.Instance.SearchCommandByEtat(nom);
                  List<Etat> etats = Sauvegarde.Instance.GetEtat();

            ViewBag.etats = etats;


            return View("GestionCommande", etatCommandes);

        }


        public IActionResult ModifieEtat(bool modifie, int id)
        {


           return  RedirectToAction("DetailsCommande", new { modifie = modifie, id = id });
        }



        public IActionResult ValidModifieEtat(bool modifie, int idEtat, int idEtatCommande)
        {
            DateTime date = DateTime.Now;

          string nom = @_accessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;

            EtatCommande etatCommande = Sauvegarde.Instance.GetEtatCommandeById(idEtatCommande);

            Etat etat = Sauvegarde.Instance.GetEtatById(idEtat);

            etatCommande.Date = date;
            etatCommande.Etat = etat;
            DataContext.Instance.SaveChanges();


            HistoriqueEtatCommandes historiqueEtatCommandes = new HistoriqueEtatCommandes() { DateEtatCommande = date, EtatComande = etat.Statut, NomEmploye = nom, NumeroCommande = etatCommande.Commande.Id };
            DataContext.Instance.HistoriqueEtatCommandes.Add(historiqueEtatCommandes);
            DataContext.Instance.SaveChanges();


            modifie = false;

            return RedirectToAction("DetailsCommande", new {modifie = modifie, id = etatCommande.Commande.Id });
        }

        public IActionResult ActionListCommande(string id)
        {


            List<EtatCommande> etatsCommandes = Sauvegarde.Instance.GetCommandeByEtat(id);

            List<Etat> etats = Sauvegarde.Instance.GetEtat();

            ViewBag.etats = etats;

            return View("GestionCommande", etatsCommandes);

        }


        public IActionResult CreateMessageCommande(int id, string message)
        {

            Commande commande = Sauvegarde.Instance.CommandeById(id);

            string email = @_accessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email).Value;

            DateTime date = DateTime.Now;

            Utilisateur utilisateur = Sauvegarde.Instance.GetUtilisateurByEmail(email);

            MessagesCommandes messagesCommandes = new MessagesCommandes() { Commande = commande, Utilisateur = utilisateur, Date = date, Message = message };

            DataContext.Instance.MessagesCommandes.Add(messagesCommandes);
            DataContext.Instance.SaveChanges();

            bool modifie = false;

            return RedirectToAction("DetailsCommande", new { modifie = modifie, id = id });

        }


        public IActionResult CreateMessageCommandeUser(int id, string message)
        {
            Commande commande = Sauvegarde.Instance.CommandeById(id);

            string email = @_accessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email).Value;

            DateTime date = DateTime.Now;

            Utilisateur utilisateur = Sauvegarde.Instance.GetUtilisateurByEmail(email);

            MessagesCommandes messagesCommandes = new MessagesCommandes() { Commande = commande, Utilisateur = utilisateur, Date = date, Message = message };

            DataContext.Instance.MessagesCommandes.Add(messagesCommandes);
            DataContext.Instance.SaveChanges();
            return RedirectToAction("DetailsCommandeUser", new { id = id });

        }


        public IActionResult HistoriqueCommandes()
        {
            

            return View();
        }


        public IActionResult GetHistoriqueCommandes(int id)
        {

            List<HistoriqueEtatCommandes> historiqueEtatCommandes = Sauvegarde.Instance.GetHistoriqueEtatCommandesByIdCommand(id);


            return View("HistoriqueCommandes",historiqueEtatCommandes );
        }


        public IActionResult DetailsCommandeUser(int id)
        {
            CommandeViewModel vm = new CommandeViewModel();

            List<Etat> etats = Sauvegarde.Instance.GetEtat();

            List<EtatCommande> listesEtatsCommandes = Sauvegarde.Instance.SearchCommandByEtatId(id);

            List<HistoriqueEtatCommandes> historiqueEtatCommandes = Sauvegarde.Instance.GetHistoriqueEtatCommandesByIdCommand(id);

            List<MessagesCommandes> messagesCommandes = Sauvegarde.Instance.GetListMessagesByIdCommande(id);

            Commande commande = Sauvegarde.Instance.GetCommandeByIdCommand(id);

            vm.EtatCommandes = listesEtatsCommandes;

            vm.Etatss = etats;

            vm.historiqueEtatCommandes = historiqueEtatCommandes;

            vm.commande = commande;

            vm.messagesCommandes = messagesCommandes;

            return View(vm);



        }

    }


}

