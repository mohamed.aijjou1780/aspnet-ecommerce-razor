﻿using ASPGestionProduit.Interface;
using ASPGestionProduit.Models;
using ASPGestionProduit.Tools;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Claims;

namespace ASPGestionProduit.Controllers
{
    public class PanierController : Controller
    {

        private IPanier _servicePanier;
        private IHttpContextAccessor _accessor;

        public PanierController(IPanier servicePabier, IHttpContextAccessor accessor)
        {
            _servicePanier = servicePabier;
            _accessor = accessor;

        }


       
        public IActionResult AffichePanier()
        {
            Panier panier = _servicePanier.GetPanier();
          
            return View(panier);


        }



        public IActionResult AddProduct(int id, int quantite)
        {


            Product product = Sauvegarde.Instance.GetProdutcById(id);
            bool trouve = false;
            Panier panier = _servicePanier.GetPanier();
          
            if (panier.ProductPaniers.Count != 0)
            {

                foreach (ProductPanier p in panier.ProductPaniers)
                {
                    if (product.Id == p.Product.Id)
                    {
                        p.Qty += quantite;
                        trouve = true;


                    }
                    else
                    {
                        trouve = false;
                    }
                }

                if (trouve == false)
                {

                    ProductPanier productPanier1 = new ProductPanier();
                    productPanier1.Product = product;
                    productPanier1.Qty = quantite;
                    _servicePanier.AddPanier(productPanier1, panier);
                }


                _servicePanier.UpdatePanier(panier);


                return RedirectToAction("AffichePanier");
            }
            else
            {
                panier = new Panier();

                ProductPanier productPanier2 = new ProductPanier();
                productPanier2.Product = product;
                productPanier2.Qty = quantite;

                _servicePanier.AddPanier(productPanier2, panier);

                return RedirectToAction("AffichePanier");



            }
        }



        public IActionResult ViderPanier()
        {

           if( _servicePanier.RemovePanier() == true)
            {
                return RedirectToAction("AffichePanier");
            }
            else
            {
                return RedirectToAction("AffichePanier");

            }

          
        }


        [Authorize(Policy = "connectOk")]
        public IActionResult etapeSuivante()
        {

            Panier panier = _servicePanier.GetPanier();
           
            if(panier.ProductPaniers.Count != 0) { 
                decimal totals = 0;
                foreach (ProductPanier p in panier.ProductPaniers)
                {
                    decimal total = p.Qty * p.Product.Prix;
                    totals += total;
                }

                ViewBag.total = totals;
            }
            else
            {
                panier = new Panier();
            }


            ViewBag.paiements = Sauvegarde.Instance.GetMoyenPaiements();
            return View("ValidationPanier", panier);




        }

    }

}
