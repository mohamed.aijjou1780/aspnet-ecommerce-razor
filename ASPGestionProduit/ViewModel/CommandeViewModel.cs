﻿using ASPGestionProduit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.ViewModel
{
    public class CommandeViewModel
    {

        public List<EtatCommande> EtatCommandes { get; set; }
        public List<Etat>Etatss { get; set; }

        public List<HistoriqueEtatCommandes> historiqueEtatCommandes { get; set; }

        public Commande commande { get; set; }

        public List<MessagesCommandes> messagesCommandes { get; set; }

    }
}
