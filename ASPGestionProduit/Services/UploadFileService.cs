﻿using ASPGestionProduit.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Services
{
    public class UploadFileService : IUploadFile
    {

        IWebHostEnvironment _env;

        public UploadFileService(IWebHostEnvironment env)
        {

            _env = env;


        }



        public string UploadFile(IFormFile file)
        {

            string path = Path.Combine(_env.WebRootPath, "upload", file.FileName);
            Stream s = System.IO.File.Create(path);
            string url = "https://localhost:44383/" + "upload/" + file.FileName;
            file.CopyTo(s);
            s.Close();
            return url;
        }


    }
}
