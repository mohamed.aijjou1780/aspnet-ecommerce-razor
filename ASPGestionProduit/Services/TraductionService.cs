﻿using ASPGestionProduit.Interface;
using ASPGestionProduit.Tools;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Services
{
    public class TraductionService : ITraduction
    {

        private IHttpContextAccessor _accessor;



        public TraductionService(IHttpContextAccessor accessor)
        {

            _accessor = accessor;


        }


        public string Traduction(string message)
        {
            string langue = _accessor.HttpContext.Session.GetString("langue");

            if(langue == null)
            {

                langue = "fr";


            }

           string nouveauMot = Sauvegarde.Instance.Traduction(message, langue);

            return nouveauMot;

        }







    }
}
