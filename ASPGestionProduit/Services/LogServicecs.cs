﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using ASPGestionProduit.Interface;

namespace ASPGestionProduit.Services
{
    public class LogServicecs : ILoger
    {
  
        public void Creationlog(string error)
        {
            string path = Directory.GetCurrentDirectory();
            string pathToFolder = Path.Combine(path, "files");
            if (!Directory.Exists(pathToFolder))
            {
                Directory.CreateDirectory(pathToFolder);
            }
            StreamWriter writerTxt = new StreamWriter(Path.Combine(pathToFolder, "fichier.txt"));
            writerTxt.Write(error);
            writerTxt.Close();


        }
    }
}
