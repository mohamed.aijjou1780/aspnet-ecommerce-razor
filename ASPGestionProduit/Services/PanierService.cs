﻿using ASPGestionProduit.Interface;
using ASPGestionProduit.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPGestionProduit.Services
{
    public class PanierService : IPanier
    {

        private IHttpContextAccessor _accessor;
        public PanierService(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }



        public Panier GetPanier()
        {

            Panier panier = new Panier();
            string panierString = _accessor.HttpContext.Session.GetString("seriePanier");

            if (panierString != null)
            {
                panier = JsonConvert.DeserializeObject<Panier>(panierString);
            }         
           return panier;
        }


        public bool RemovePanier()
        {

            _accessor.HttpContext.Session.Remove("seriePanier");
            string panierString = _accessor.HttpContext.Session.GetString("seriePanier");


            if (panierString == null)
            {

                return true;
            }
            else
            {
                return false;
            }

        }


        public bool RemoveProductPanier(ProductCommande productCommande)
        {

            return true;
        }


        public Panier AddPanier(ProductPanier productPanier, Panier panier)
        {

            panier.ProductPaniers.Add(productPanier);

            int nmbreProduct = panier.ProductPaniers.Count();

           _accessor.HttpContext.Session.SetInt32("qteProduct", nmbreProduct);

            string panierString = JsonConvert.SerializeObject(panier);

            _accessor.HttpContext.Session.SetString("seriePanier", panierString);


            return panier;
           
        }

        public void UpdatePanier(Panier panier)
        {
            int nmbreProduct = panier.ProductPaniers.Count();

            _accessor.HttpContext.Session.SetInt32("qteProduct", nmbreProduct);

            string panierString = JsonConvert.SerializeObject(panier);

            _accessor.HttpContext.Session.SetString("seriePanier", panierString);


        }


     

    }
}
